import java.util.*;

/**
 * @author Liya Norng
 * @version 1.0
 * @since 1/20/2016
 */

/**
 * this is the AddressBook class
 * it take the AddressEntry and put it in the
 * map
 */
public class AddressBook {
    /**
     * @param map_List is all the entry of AddressEntry with the key.
     */
    private Map<String,AddressEntry> map_List = new HashMap<String, AddressEntry>();
    ArrayList<String> nums = new ArrayList<String>();
    private String first_Name;
    private String last_Name;
    
    
    /**
     * getting all the information about the person
     * @param first_Name
     * @param last_Name
     * @param street_Name
     * @param city_Name
     * @param state_Name
     * @param zip_Name
     * @param email_Name
     * @param phone_Name
     * @param contact is the instance of AddressEntry class
     * @param key_Name is string for the key to access all the contacts
     * adding all the key name and the contacts to the map.
     */
    public void newEntry(String first_Name, String last_Name, String street_Name, String city_Name, String state_Name, String zip_Name, String email_Name, String phone_Name)
    {
    	// adding all the info of one person at a time to the addressentry, and from there i call map to add in the addressentry
        AddressEntry contact = new AddressEntry(first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
        String key_Name = email_Name;
        key_Name.toLowerCase();
        map_List.put(key_Name, contact);
    }
    
    /**
     * this would loop through the map, and get the key, and access the map.
     * then, print it out to the user window
     * @param running_Total is for counting how many contact list.
     * @param info_List  is an instance of AddressEntry class for putting all the entries into the string inside 
     * AddressEntry class
     */
    public void getList(int print)
    {
    	int running_Total = 0;
        for (String key: map_List.keySet())
        {
    		AddressEntry info_List = map_List.get(key);
        	
    		// I use this if statement printing out in differnece format. I didn't want to create another method.
    		if (print == 0)
        	{
                System.out.println(info_List.toString());
                System.out.print("\n");
        	}
        	else
        	{
        		running_Total++;
                System.out.print(running_Total);
                System.out.println(":");
                System.out.println(info_List.toString());
                System.out.print("\n");
        	}
        }
    }
    
    /**
     * 
     * @return map_List this is a Map that contains all the contact. It will return to the AddressBookApplication
     */
    public Map<String,AddressEntry> getMap()
    {
    	// returning the map to the addressBookApplication.
        return (map_List);
    }
    
    /**
     * 
     * @param name is the slot of the array, that the key is store in.
     * From there I get the key to the Map, so I can remove the entry.
     */
    public void remove(int name)
    {
    	// when i got the number of the slot that the use pick, I call the array to get the key, and from there i call map to remove.
    	map_List.remove(nums.get(name - 1));
    }
    
    /**
     * 
     * @return map_List is a Map. I use this to check if the Map is empty Map.
     */
    public boolean check()
    {
        return (map_List.isEmpty());
    }
    
    /**
     * 
     * @param name is the last name of the person
     * @return running_Total is the counting of how many entry there are in the Map
     * so this is the counting the name that matches in the Map
     */
    public int count(String name)
    {
        name.toLowerCase();
        char c = name.charAt(0);
        int running_Total = 0;
        for (String key: map_List.keySet())
        {
            AddressEntry info_List = map_List.get(key);
            if (name.equals(info_List.getLastName()))
            {
            	// when it found the contact, it will store those name in the array.
                running_Total++;
                nums.add(key);
            }
            else if (info_List.getLastName().isEmpty())
            {
            }
            else
            {
                String lowerCase = info_List.getLastName().toLowerCase();
                if (c == lowerCase.charAt(0))
                {
                    running_Total++;
                    nums.add(key);
                }
            }
        }
        // return the number of contact that's found in the map
        return (running_Total);
    }
    
    /**
     * 
     * @return first_Name it return the first name of the person. I use this for the AddessBookApplication.
     */
    public String getFirstNames()
    {
    	// return the first name that use for when the user pick the name to delete, and this goes the same to getLastName().
        return first_Name;
    }
    
    /**
     * 
     * @return last_Name it return the last name of the person. I use this for the AddessBookApplication.
     */
    public String getLastNames()
    {
        return last_Name;
    }
    
    /**
     * this method is to clear the arrays, when I'm done with checking the contact
     */
    public void getCLear()
    {
    	nums.clear();
    }
    
    /**
     * 
     * @param name is the last name of the person for checking
     * @param select it for the value that matches of the person in the list
     * @param c is a char that use to check if the user input only a char.
     * @param running_Total is use for counting.
     * @param check is for checking if the following if statement was execute. If not it will give an error to the user
     * saying that it did not find the contact
     * 
     * so this is almost the same as the count() method. 
     */
    public void getContact(String name, int select)
    {
        name.toLowerCase();
        char c = name.charAt(0);
        int running_Total = 0;
        int check = 0;
        
        // doing a for loop to get all the key in the map
        for (String key: map_List.keySet())
        {
        	// setting all the info in the key valueto the addressEntry, so I can call addressEntry to return me a string.
            AddressEntry info_List = map_List.get(key);
            if (select == 0)
            {
            	// checking if the name matches the name in the map
                if (name.equals(info_List.getLastName()))
                {
                    check = 1;
                    running_Total++;
                    System.out.print(running_Total);
                    System.out.println(":");
                    System.out.println(info_List.toString());
                }
                else if (info_List.getLastName().isEmpty())
                {
                }
                else
                {
                	// converting the string to lower case
                    String lowerCase = info_List.getLastName().toLowerCase();
                   
                    // this is checking if the user actually type in a char rather than a string
                    if (c == lowerCase.charAt(0))
                    {
                        check = 1;
                        running_Total++;
                        System.out.print(running_Total);
                        System.out.println(":");
                        System.out.println(info_List.toString());
                    }
                }
            }
            else if (select > 0)
            {
            	// this is for when the user decide to pick a name to delete, and i use this to store the first, and last name to the string up abouve.
            	// so i can call back to this to print out the name that got delete it.
            	running_Total++;
                if (nums.get(select - 1 ) == key)
                {
                    System.out.println(info_List.toString());
                    first_Name = info_List.getFirstName();
                    last_Name = info_List.getLastName();
                    check = 1;
                }
            }
        }
        if (check == 0)
        {
        	// this is checking if there is any contact found in the map
            System.out.println("There is currently no contact were found.");
            check = 0;
        }
    }	
}
