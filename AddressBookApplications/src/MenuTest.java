import static org.junit.Assert.*;

import org.junit.Test;

public class MenuTest {

	@Test
	public void testMain() {
	}

	@Test
	public void testFirstName() {
		Menu a = new Menu();
		assertEquals("First Name: ", a.FirstName());
		
		
	}

	@Test
	public void testLastName() {
		Menu a = new Menu();
		assertEquals("Last Name: ", a.LastName());
	}

	@Test
	public void testStreet() {
		Menu a = new Menu();
		assertEquals("Street: ", a.Street());
	}

	@Test
	public void testCity() {
		Menu a = new Menu();
		assertEquals("City: ", a.City());
	}

	@Test
	public void testState() {
		Menu a = new Menu();
		assertEquals("State: ", a.State());
	}

	@Test
	public void testZip() {
		Menu a = new Menu();
		assertEquals("Zip: ", a.Zip());
	}

	@Test
	public void testTelephone() {
		Menu a = new Menu();
		assertEquals("Telephone: ", a.Telephone());
	}

	@Test
	public void testEmail() {
		Menu a = new Menu();
		assertEquals("Email: ", a.Email());
	}

}
