import static org.junit.Assert.*;

import org.junit.Test;

public class AddressBookApplicationTest {

	@Test
	public void testInit() {
		AddressBookApplication a = new AddressBookApplication();
		AddressBook contact = new AddressBook();
		int s = a.init("file.txt", contact);
		assertEquals(s, 5);
		int t = a.init("resultingnewdatafile.txt", contact);
		assertEquals(t, 5);	
		}

}
