import java.util.*;
import java.io.*;

/**
 * 
 * @author Liya Norng
 * @version 1.0
 * @since 1/20/2016
 *
 */

/**
 * This is the parent of all of the classes.
 * This would call to anothe class for the answer.
 *
 */
public class AddressBookApplication {
	
	public static int file_Open = 0;


	/**
	 * This is the main, where it begin first.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * This is for exercise 1.
		 * it create an instance of class Menu.
		 * then, it call for FileName(), LastName()...
		 * @param prompt is the instance of Menu class, it for getting the all the entries that ask the user for information.
		 */
	    //System.out.println("Hellow Cs class");
		Menu prompt = new Menu();
		//System.out.println(prompt.FirstName());
	    //System.out.println(prompt.LastName());
		//System.out.println(prompt.Street());
	    //System.out.println(prompt.City());
	    //System.out.println(prompt.State());
	    //System.out.println(prompt.Zip());
		//System.out.println(prompt.Telephone());
		//System.out.println(prompt.Email());
		
		
		/**
		 * this is for exercise 3
		 * it create an instance of class AddressBookApplication.
		 * then, it call for initAddresssBookExercise() to print out
		 * the contacts.
		 * @param a is the instance of AddressBookApplication class
		 */
		AddressBookApplication a = new AddressBookApplication();
		//a.initAddressBookExercise();
		
		/**
		 * this is for exercise 7
		 * create an object class for input from the user.
		 * it prompt the user for file name.
		 * then, store it in the file_Name, and call the init() method.
		 * @param input is the instance class for User input
		 * @param file_Name is the file name
		 */
		AddressBook contact = new AddressBook();
	    Scanner input = new Scanner(System.in);
		//System.out.println("Please give a file name for reading the contact: ");
		//String file_Name = null;
		//file_Name = input.nextLine();
		//a.init(file_Name, contact);
		
	    /**
	     * @param count_Total is for the count of how many entries was read
	     * @param prompt is a string for getting user input to check which one is user request
	     * @param user_Input is a string for getting user input for completing the rest of the requirement for each section.
	     * @param person_Info is an instance of AddressEntry class for getting the input for each person.
	     * @param entry is an int for getting the user input for checking which contact need to delete.
	     * So here is a while loop that is always true until user pick option f which is save and quit. This while loop will
	     * loop and prompt the user for certain options. Those options are a) Loading from file, b) Addition, c) Removal, d) Find, 
	     * e)Listing, and f) Save and Quit. So when the user pick one of those options, it will then guy the user to get the answer for the user.
	     * 
	     * So here is a short summary of how the code run in each option.
	     * a) 	it will prompt the user, and call the init() method to open the file.
	     * b) 	it will prompt the user for information of the new contact. To get all of that uses @param person_Info to call to the AddressEntry() class.
	     * then, after putting all the information into all the string in AddressEntry, and from there it will call to AddressBook to add into the Map, 
	     * it will then call the method to add. Then, call to method toString to print out the contact that just added.
	     * c) 	it will prompt the user for what name to delete. Then, from there it will list the name that user request. If the user pick the appropriate categories,
	     * it will call the AddressBook to remove the contact. 
	     * d)	prompt the user the name to find in the Map, which it has all the entries of all contact. Then from there it will 
	     * call AddressBook class to list the contact that matches the user request.
	     * e)	it will call to AddressBook class to list all the contacts
	     * f)	It will prompt the user for the name of the file to store. Then from there it will then create the stream output, and send out the data
	     * to the file. and quit the AddressBookApplication.
	     * 
	     */
		int count_Total = 0;
		String prompt1= null;
		String user_Input = null;
		AddressEntry person_Info = new AddressEntry();
		int entry = 0;
		while (true)
		{
			// prompting user for the category that they have available for them.
			System.out.println("*****************************************************\n");
			System.out.println("Please enter in your menu selection:\n");
			System.out.println("a) Loading From File\n");
			System.out.println("b) Addition\n");
			System.out.println("c) Removal\n");
			System.out.println("d) Find\n");
			System.out.println("e) Listing\n");
			System.out.println("f) Save and Quit\n");
			System.out.println("*****************************************************\n");
			
			//getting user input and from there I use the user input to coampre with my if statement to see which category does it fit in.
			prompt1 = input.nextLine();
			if (prompt1.equals("a") || prompt1.equals("A"))
			{
				// getting the file name to read the contacts to the AddressBook
				System.out.println("Enter in FileName: ");
				user_Input = input.nextLine();
				// calling the read file, to get all the contacts store in addressbook, and get the return, the number of contacts,
				// and store it in count_Total.
				count_Total = a.init(user_Input,contact);
				System.out.print("\n");
				
				// this if statemen is checking if the user actually open the file successfully, if not it will not print this.
				if (file_Open == 1)
				{
					// prompting the user how many contacts it have read successfully.
					System.out.print("Read in ");
					System.out.print(count_Total);
					System.out.print(" new Addresses, successfullly loaded, currently ");
					System.out.print(count_Total);
					System.out.println(" Addresses.");
				}
			}
			else if (prompt1.equals("b") || prompt1.equals("B"))
			{
				// prompting the user the firstname, and get the user info and store it in addressentry.
				System.out.println(prompt.FirstName());
				user_Input = input.nextLine();
				person_Info.setFirstName(user_Input);
				
				// prompting the user the lastname and get the user info and store it in addressentry
				System.out.println(prompt.LastName());
				user_Input = input.nextLine();
				person_Info.setLastName(user_Input);
				
				// prompting the user the street and get the user info and store it in addressentry
				System.out.println(prompt.Street());
				user_Input = input.nextLine();
				person_Info.setStreet(user_Input);
					
				// prompting the user the city and get the user info and store it in addressentry
				System.out.println(prompt.City());
				user_Input = input.nextLine();
				person_Info.setCity(user_Input);
					
				// prompting the user the state and get the user info and store it in addressentry
				System.out.println(prompt.State());
				user_Input = input.nextLine();
				person_Info.setState(user_Input);
				
				// prompting the user the zip and get the user info and store it in addressentry
				System.out.println(prompt.Zip());
				user_Input = input.nextLine();
				person_Info.setZip(user_Input);
					
				// prompting the user the phone and get the user info and store it in addressentry
				System.out.println(prompt.Telephone());
				user_Input = input.nextLine();
				person_Info.setPhone(user_Input);
					
				// prompting the user the email and get the user info and store it in addressentry
				System.out.println(prompt.Email());
				user_Input = input.nextLine();
				person_Info.setEmail(user_Input);
				
				// this I call to the method in addressEntry to addcontact, and from there I create an instance of addressBook class,
				// and call the newentry in the addressBook class. Since I have all the info of one person, I can add that entries to the addressBook.
				person_Info.addContact(contact);

				// print out the contact that I just added it
				System.out.print("Thank you the following contact has been added to your address book:\n");
				System.out.println(person_Info.toString());
				file_Open = 1;
			}
			else if (prompt1.equals("c") || prompt1.equals("C"))
			{
				// this file_open is for checking if they open the file yet, if not they need to open the file before they even continue on
				if (file_Open == 1)
				{
					// prompting the user, and get the last name from the user
					System.out.print("Enter in Last Name of contact to remove:\n");
					user_Input = input.nextLine();
					
					// I call count() in addressBook to count the contact that matches the user request.
					int running_Total = contact.count(user_Input);
					
					// this is checking if there was no name was found then it will re prompt the user to type in the name again
					if (running_Total == 0)
					{
						while (true)
						{
							System.out.print("Sorry, there no match with \"");
							System.out.print(user_Input);
							System.out.println("\". Please try again with a different name.");
							System.out.print("Enter in Last Name of contact to remove:\n");
							user_Input = input.nextLine();
							running_Total = contact.count(user_Input);
							
							// if it find the name in the contact list it will end the loop.
							if (running_Total != 0)
							{
								break;
							}
						}
					}
					
					// prompting the user the following list that's found in addressBook
					// it will list the contact in a nicely format
					System.out.print("The following ");
					System.out.print(running_Total); 
					System.out.print(" entries were found in the address book, select number of entry you wish to remove:\n");
					contact.count(user_Input);
					contact.getContact(user_Input, 0);
					
					// this is checking to make sure that the user did not type in a string rather than an int.
					// if the user type in a string, it will loop again, and ask for an int.
					try
					{
						entry = input.nextInt();
						while (true)
						{
							// this outside if statement is double checking the user input to make sure they pick one from the list
							if (entry > 0 && entry <= running_Total)
							{
								System.out.print("Hit y to remove the following entry or n to return to the main menu:\n");
								contact.getContact(user_Input, entry);
								user_Input = input.nextLine();
								user_Input = input.nextLine();
								if (user_Input.equals("y") || user_Input.equals("Y"))
								{
									// this will print out to user that it is successfully delete, and will end the loop
									contact.remove(entry);
									System.out.print("You have successfully removed the ");
									System.out.print(contact.getFirstNames()); 
									System.out.print(" "); 
									System.out.print(contact.getLastNames()); 
									System.out.println(" contact. ");
									break;
								}
								else
								{
									// if the user change their mind not deleting, it will end the loop
									System.out.println("You did not remove any contact.");
									break;
								}
							}
							else
							{
								System.out.println("Sorry, your request didn't match the following contact list.");
								System.out.println("Please double check your request again.");
								entry = input.nextInt();
							}
						}
					}
					catch (java.util.InputMismatchException e)
				    {
						// if the user type in string this would print out
					     System.out.println("Invalid Input. Please try again.");
				    }
				}
				else
				{
					System.out.println("Please open the file first before you start removing contact.");
					System.out.println("Please select 'a' to get start loading the file. Thank you.");
				}
				
				// calling to addressBook to clear the array that I use to keep all the info that the user was looking for when to delete.
				contact.getCLear();
			}
			else if (prompt1.equals("d") || prompt1.equals("D"))
			{
				if (file_Open == 1)
				{
					// prompting the user, and listing the contact that are in the AddressBook
					System.out.println("Enter in all or the begining of the Last Name of the contact you wish to find:");
					user_Input = input.nextLine();
					System.out.print("The following ");
					System.out.print(contact.count(user_Input));
					System.out.print(" entries were found in the address book for a last name starting with \"");
					System.out.print(user_Input);
					System.out.print("\" :\n");
					contact.getContact(user_Input, 0);
				}
				else
				{
					System.out.println("Please open the file first before you start finding contact.");
					System.out.println("Please select 'a' to get start loading the file. Thank you.");
				}
			}
			else if (prompt1.equals("e") || prompt1.equals("E"))
			{
				if (file_Open == 1)
				{
					// this if statement is calling to addressBook to make sure this is conacts to list
					if (contact.check())
					{
						System.out.println("Sorry, there is no contact.");
					}
					else
					{
						// calling to addressBook to list the contact
						contact.getList(1);
					}
				}
				else
				{
					System.out.println("Please open the file first before you start listing the contact.");
					System.out.println("Please select 'a' to get start loading the file. Thank you.");
				}
			}
			else if (prompt1.equals("f") || prompt1.equals("F"))
			{
				if (file_Open == 1)
				{
					/**
					 * @param fileWriter is an instance of class for filewriter
					 * @param bufferedwriter is an instance of class for buffer when reading and writing.
					 * @param name is a Map that also work as a list, but this one have a key to access the item.
					 * @param info_List is an instance of AddressBook class to store the information appropriate to the string name.
					 * 
					 * So basically, I use AddressEntry to add in all the information about the person. Then from there, it will 
					 * get those input to write it to the file.
					 */
					System.out.println("Enter in the name of the file you wish to save your AddressBook to:\n");
					user_Input = input.nextLine();
			        try 
			        {
			        	// creating the filewriter,and bufferedwriter to write to the file.
			        	FileWriter fileWriter = new FileWriter(user_Input);
			        	BufferedWriter buffer = new BufferedWriter(fileWriter);
			        	Map<String,AddressEntry> name = contact.getMap();
			        	
			        	// creating a for loop to get the key in the map.
			        	for (String key: name.keySet())
			        	{
			        		// so when i got the key i can access to it, and from there i can call the addressEntry class to 
			        		// get all the entry to write to a file.
			        		// the way i write to file is to get a line of string, and from there i write a line to the file
			    			AddressEntry info_List = name.get(key);
			    			buffer.write(info_List.getFirstName());
			    			buffer.newLine();
			    			buffer.write(info_List.getLastName());
			    			buffer.newLine();
			    			buffer.write(info_List.getStreet());
			    			buffer.newLine();
			    			buffer.write(info_List.getCity());
			    			buffer.newLine();
			    			buffer.write(info_List.getState());
			    			buffer.newLine();
			    			buffer.write(info_List.getZip());
			    			buffer.newLine();
			    			buffer.write(info_List.getEmail());
			    			buffer.newLine();
			    			buffer.write(info_List.getPhone());
			    			buffer.newLine();
			    		}
			        	
			        	// prompting the user that the contacts successfully write to the file.
			        	// closing the buffer, and file.
			        	System.out.println("Thank you, your AddressBook has been backed up to the file\n");
			    		buffer.close();
						input.close();
						break;
			        } 
			        catch ( IOException e ) 
			        {
			            e.printStackTrace();
			        } 			
				}
				else
				{
					// this prompt is for when the user did not pick option A to load the file to get all the contact to the addressBook
					System.out.println("Please open the file first before you start save contact, and quit.");
					System.out.println("Please select 'a' to get start loading the file. Thank you.");
				}
			}
			else
			{
				// this prompt is when the user is not picking one of this category.
				System.out.println("Please choose 'a, b, c, d, e, or f' as your answer.\n");
			}
		}
	}
	
	/**
	 * this is part of exercise 3
	 * it an instance of AddressBook class.
	 * then, call the method newEntry() to add in the contacts.
	 * After done adding all the contact, I call the getList() method to print 
	 * to the user.
	 * @param contact is instance of AddressBook class
	 */
	public void initAddressBookExercise()
	{
		AddressBook contact = new AddressBook();
		contact.newEntry ("Liya", "Norng", "311 B street", "Tracy","CA.","95376", "Liya_Norng@yahoo.com", "(209) 346-2998");
		contact.newEntry("Kado", "Norng", "311 B street", "Tracy","CA.","95376", "kadonorng@yahoo.com", "(209) 346-2998");
		contact.getList(1);
	}
	
	/**
	 * this is part of exercise 7
	 * @param contact is the instance AddressBook class
	 * @param first_Name is first name
	 * @param last_Name is last name
	 * @param street_Name is street name
	 * @param city_Name is city name
	 * @param state_Name is state name
	 * @param zip_Name is the zip code
	 * @param email_Name is the email
	 * @param phone_Name is the phone number
	 * @param line is the string that will read from the file a line at a time
	 * @param file_Input is the instance of FileReader class.
	 * @param buffer is the instance of BufferedReader class.
	 * @param running_Total is for checking the location of the file.
	 * so this method would try to open the file that the user asking for.
	 * then, it would loop, and read the file line at a time. While looping, it will
	 * store the string line to the specific string name, and it will know by the if statement.
	 * when it get to runningTotal get to 7, it will reset the runningTotal. Then
	 * call the method newEntry() to add in new contacts, and print out the contacts by calling
	 * the method getList().
	 * Also, I use try, and catch to check if the open, and reading the file go accordingly.
	 */
	public int init(String fileName, AddressBook contact)
	{	
		
		// this is all the string for store all the entry. 
		String first_Name = null;
		String last_Name = null;
		String street_Name = null;
		String city_Name = null;
		String state_Name = null;
		String zip_Name = null;
		String email_Name = null;
		String phone_Name = null;
		String line = null;
		int count = 0;
		try
		{
			// this is to read the file, and from there it store to the string above for temperary.
			FileReader file_Input = new FileReader(fileName);
			BufferedReader buffer = new BufferedReader(file_Input);
			int running_Total = 0;
           
			// this is a while loop, and reading a line at a time, and store each line to appropriate area.
			while((line = buffer.readLine()) != null) 
			{
            	if (running_Total == 0)
				{
					first_Name = line;
					running_Total++;
				}
            	else if (running_Total == 1)
				{
            		last_Name = line;
					running_Total++;
				}
            	else if (running_Total == 2)
				{
					street_Name = line;
					running_Total++;
				}
            	else if (running_Total == 3)
				{
            		city_Name = line;
					running_Total++;
				}
            	else if (running_Total == 4)
				{
            		state_Name = line;
					running_Total++;
				}
            	else if (running_Total == 5)
				{
            		zip_Name = line;
					running_Total++;
				}
            	else if (running_Total == 6)
				{
					email_Name = line;
					running_Total++;
				}
            	else if (running_Total == 7)
				{
					phone_Name = line;
					
					// since the contact format is 7 lines, then when it in line 7, it will call this newEntry method to adding the contact to the map
					contact.newEntry(first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
					
					// resetting the running_Total to 0, because it will start all over again.
					running_Total = 0;
					
					// this is the running total for counting the contact that added to the list.
					count = count + 1;
					
					// this file_open is for checking for if the file open successfully
					file_Open = 1;
				}
			}
            buffer.close();
		}
		catch (IOException ex)
		{
			System.out.println("Sorry, file '" + fileName + "' does not exist.");
			file_Open = 0;
		}
        return (count);
	}
}
